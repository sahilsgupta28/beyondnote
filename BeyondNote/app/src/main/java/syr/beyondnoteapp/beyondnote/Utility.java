package syr.beyondnoteapp.beyondnote;

import android.icu.text.SimpleDateFormat;
import android.icu.util.Calendar;

/**
 * Created by Sahil on 4/18/2017.
 */

public class Utility {

    static String GetData()
    {
        String strDate;
        Calendar c = Calendar.getInstance();

        SimpleDateFormat sdf_date = new SimpleDateFormat("MM/dd/yyyy");
        strDate = sdf_date.format(c.getTime());

        return strDate;
    }

    static String GetTime()
    {
        String strTime;
        Calendar c = Calendar.getInstance();

        SimpleDateFormat sdf_time = new SimpleDateFormat("HH:mm");
        strTime = sdf_time.format(c.getTime());

        return strTime;
    }
}
