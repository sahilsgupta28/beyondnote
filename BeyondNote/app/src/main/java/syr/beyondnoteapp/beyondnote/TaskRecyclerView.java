package syr.beyondnoteapp.beyondnote;

/**
 * TaskRecyclerView class
 *
 * @author: Omkar Patil
 * @date: 13 April 2017
 * @email: ospatil@syr.edu
 */

import android.content.Intent;
import android.os.PersistableBundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

public class TaskRecyclerView extends AppCompatActivity {
    TextView title;
    Toolbar colorBar;
    FloatingActionButton addtask;
    String UserId;
    Fragment mcontent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_task_recycler_view);

        //returned intent that started this activity
        Intent intentExtras = getIntent();
        UserId = intentExtras.getStringExtra("email");

        //toolbar for sort, search
        Toolbar toolbar = (Toolbar) findViewById(R.id.task_action_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        title = (TextView) findViewById(R.id.tvToolBarTitle);
        title.setText("Task List");

    // Replacing framelayout of task with recycler view fragment
        getSupportFragmentManager().beginTransaction().
                add(R.id.container2, TaskRecyclerFragment.newInstance(R.id.taskrecyclerFrag,UserId),"TagRecycleforAddTask")
                .commit();
    }


}
