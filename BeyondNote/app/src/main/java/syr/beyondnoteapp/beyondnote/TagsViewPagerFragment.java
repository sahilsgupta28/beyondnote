package syr.beyondnoteapp.beyondnote;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;
import java.util.Map;

public class TagsViewPagerFragment extends Fragment {

    private static final String ARG_EMAIL = "EMAIL";
    private String mUserEmail;

//    private OnFragmentInteractionListener mListener;

    ViewPager viewPager;
    TagsViewPagerAdapter VpAdapter;
    Database myDatabase;

    public TagsViewPagerFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment TagsViewPagerFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static TagsViewPagerFragment newInstance(String userEmail) {
        TagsViewPagerFragment fragment = new TagsViewPagerFragment();
        Bundle args = new Bundle();
        args.putString(ARG_EMAIL, userEmail);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mUserEmail = getArguments().getString(ARG_EMAIL);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_tags_view_pager, container, false);

        myDatabase = new Database(mUserEmail);
        myDatabase.initializeDataFromCloud();

        for (Map<String, ?> myMap : myDatabase.mTagsList) {
            System.out.println("Adding Note: " + myMap.get("title"));
        }

        viewPager = (ViewPager) rootView.findViewById(R.id.tags_view_pager);
        VpAdapter = new TagsViewPagerAdapter(((AppCompatActivity)getActivity()).getSupportFragmentManager(), myDatabase.mTagsList);
        viewPager.setAdapter(VpAdapter);

        viewPager.setPageTransformer(false, new ViewPager.PageTransformer() {
            @Override
            public void transformPage(View page, float position) {
                final float pos = Math.abs(Math.abs(position) - 1);
                page.setScaleX(pos / 2 + 0.5f);
                page.setScaleY(pos / 2 + 0.5f);

                page.setRotationY(position * -30);
            }
        });

        TabLayout VpTabLayout = (TabLayout) rootView.findViewById(R.id.tags_tab_layout);
        VpTabLayout.setupWithViewPager(viewPager);

        return rootView;
    }

//    public void onButtonPressed(Uri uri) {
//        if (mListener != null) {
//            mListener.onFragmentInteraction(uri);
//        }
//    }
//
//    @Override
//    public void onAttach(Context context) {
//        super.onAttach(context);
//        if (context instanceof OnFragmentInteractionListener) {
//            mListener = (OnFragmentInteractionListener) context;
//        } else {
//            throw new RuntimeException(context.toString()
//                    + " must implement OnFragmentInteractionListener");
//        }
//    }
//
//    @Override
//    public void onDetach() {
//        super.onDetach();
//        mListener = null;
//    }
//
//    /**
//     * This interface must be implemented by activities that contain this
//     * fragment to allow an interaction in this fragment to be communicated
//     * to the activity and potentially other fragments contained in that
//     * activity.
//     * <p>
//     * See the Android Training lesson <a href=
//     * "http://developer.android.com/training/basics/fragments/communicating.html"
//     * >Communicating with Other Fragments</a> for more information.
//     */
//    public interface OnFragmentInteractionListener {
//        void onFragmentInteraction(Uri uri);
//    }
}
